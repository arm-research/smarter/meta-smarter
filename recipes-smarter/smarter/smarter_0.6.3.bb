SUMMARY = "SMARTER recipe"
DESCRIPTION = "Install SMARTER stack and first-run script which associates node with cloud-side resources"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit deploy

SRCREV="v0.6.0"
SRC_URI = "file://smarter-firstrun.sh \
           file://smarter.cfg \
           file://alsa-base.conf"

S = "${WORKDIR}"

do_compile() {
}

do_install() {
    install -d ${D}${bindir}
    install -d ${D}/smarter
    install -m 0755 ${S}/smarter-firstrun.sh ${D}${bindir}
    install -d ${D}/${sysconfdir}/modprobe.d
    install -m 0755 ${S}/alsa-base.conf ${D}${sysconfdir}/modprobe.d
}

do_deploy() {
    cp smarter.cfg ${DEPLOYDIR}
}

pkg_postinst_ontarget_${PN}() {
    ${bindir}/smarter-firstrun.sh
}

FILES_${PN} += "/smarter ${sysconfdir}/modprobe.d/alsa-base.conf"


RDEPENDS_smarter = "bash"