#!/bin/bash
# Copyright (c) 2020, Arm Ltd
# Based on original by author: Luis E. P.
# Modified for yocto by: Eric Van Hensbergen
#
# This script will configure k3s and join this node to k3s master
# This script will setup default hostname

echo "Initializing SMARTER..."

# Create logs directory if doesn't exist...
mkdir -p /smarter/logs
touch /var/log/lastlog

# Create persistent directory for faas
mkdir -p /smarter/faas
chmod -R ugo+rw /smarter/faas

# Use the last 8 digits of the MAC address as an identifier. 
export LAST8=$(cat /sys/class/net/eth0/address | awk -F":" '{print $3$4$5$6}')

# Setup persistent storage for rancher and pivot to using it
echo "   Setting up persistent disk parition"
if [ ! -d "/persistent" ]; then
    mkdir /persistent
fi

BYLABEL="/dev/disk/by-label"

# Correct for differences in various linux kernel flavors
if [ ! -e $BYLABEL ]; then
    BYLABEL="/dev/disk/by-partlabel"
fi

if [ -e $BYLABEL/UDA ]; then
    if (! mount $BYLABEL/UDA /persistent ); then
        mkfs.ext4 $BYLABEL/UDA
        mount $BYLABEL/UDA /persistent
        echo PARTLABEL=UDA /persistent ext4 defaults 0 1 >> /etc/fstab
    fi
fi


if [ ! -e /persistent/rancher ]; then
    mkdir -p /persistent/rancher
fi
rm -rf /var/lib/rancher
ln -s /persistent/rancher /var/lib/rancher



echo "   Reading configuration"
# TODO: need to do a multi-step process
#   - first check and see if there is a boot.cfg txt with the right signature
#   - then check for a partition
#   - if partition exists, make sure it has the right signature (and is null terminated!)
#   - if neither exists, maybe we just stop here
# Read flash-time configuration

if [ -e $BYLABEL/SMARTER-CFG ]; then
    col -b < $BYLABEL/SMARTER-CFG > /tmp/smarter.bash 
else
    cp /boot/smarter.cfg /tmp/smarter.bash
fi

if [ ! -e /tmp/smarter.bash ]; then
    echo "SMARTER: WARNING: No Configuration File Found, Nothing Configured"
fi

. /tmp/smarter.bash

if [ -z "${FACTORY_MODDE}" ]; then
    echo "Factory mode disabled"
else
    touch /tmp/factory-mode
fi

NODETYPE=`hostname | cut -f1 -d"-"`
MODELTYPE=`hostname | cut -f2 -d"-"`

# Expand btrfs partition to full remaining capacity of SD card
# TODO: we may need to conditionalize PART_NUM and storage device to other platforms
if [ ! $NODETYPE = "jetson" ]; then
    umount /persistent
    PART_NUM=3
    PART_START=$(fdisk -l | grep /dev/mmcblk0p3 | awk '{print $2}')
    fdisk /dev/mmcblk0 <<EOF
p
d
$PART_NUM
n
p
$PART_NUM
$PART_START

p
w
EOF
    resize2fs /dev/mmcblk0p3
    mount /dev/mmcblk0p3 /persistent
    #btrfs filesystem resize max /persistent
fi


# Set hostname
if [ -z "${FORCE_HOSTNAME}" ]; then
    echo smarter-`hostname`-$LAST8 > /etc/hostname
else
    echo ${FORCE_HOSTNAME} > /etc/hostname
fi
hostname `cat /etc/hostname`

# Set ssh key up
if [ -z "${SSHKEY}" ]; then
    echo "WARNING: No ssh key set"
else
    mkdir -p /home/root/.ssh
    echo $SSHKEY > /home/root/.ssh/authorized_keys
fi

# Set root passwd up
if [ -z ${PASSWD} ]; then
    echo "WARNING: No root password set, system may be insecure!"
else
    echo root:$PASSWD | chpasswd
fi

if [ -z ${K3S_SERVER_PORT} ]; then
    K3S_SERVER_PORT=6443
fi

if [ ! -z ${K3S_SERVER_IP} ]; then
    # Install K3S
    K3S_TOKEN="$K3S_TOKEN"
    INSTALL_K3S_EXEC="--no-flannel"
    cat > /etc/default/k3s << END
mount --make-rshared -o remount /
modprobe vhost-net
INSTALL_K3S_SKIP_DOWNLOAD="true"
K3S_ARGS="agent \
            --log /var/log/k3s.log \
            --kubelet-arg cluster-dns=169.254.0.2 \
            --kubelet-arg root-dir=/persistent/kubelet \
            --node-label smarter-device-manager=enabled \
            --node-label smarter.cni=deploy \
            --node-label smarter.cri=containerd \
            --node-label smarter.nodetype=$NODETYPE \
            --node-label smarter.nodemodel=$MODELTYPE \
            --node-label smarter.serial=$LAST8 \
            --node-label smarter.type=edge \
            --node-taint smarter.type=edge:NoSchedule \
            --node-label smarter-build=yocto \
            --no-flannel \
            --token $K3S_SERVER_TOKEN \
            --server https://$K3S_SERVER_IP:$K3S_SERVER_PORT"
END

# Install template for containerd configuration
mkdir -p /var/lib/rancher/k3s/agent/etc/containerd
cat > /var/lib/rancher/k3s/agent/etc/containerd/config.toml.tmpl  << END
[plugins.opt]
  path = "{{ .NodeConfig.Containerd.Opt }}"
[plugins.cri]
  stream_server_address = "127.0.0.1"
  stream_server_port = "10010"
{{- if .IsRunningInUserNS }}
  disable_cgroup = true
  disable_apparmor = true
  restrict_oom_score_adj = true
{{end}}
{{- if .NodeConfig.AgentConfig.PauseImage }}
  sandbox_image = "{{ .NodeConfig.AgentConfig.PauseImage }}"
{{end}}
{{- if not .NodeConfig.NoFlannel }}
[plugins.cri.cni]
  bin_dir = "{{ .NodeConfig.AgentConfig.CNIBinDir }}"
  conf_dir = "{{ .NodeConfig.AgentConfig.CNIConfDir }}"
{{end}}


[plugins.cri.containerd]
  disable_snapshot_annotations = true
  snapshotter = "overlayfs"

[plugins.cri.containerd.runtimes.runc]
  runtime_type = "io.containerd.runc.v2"

[plugins.cri.containerd.runtimes.runc.options]
  BinaryName = "nvidia-container-runtime"

[plugins.cri.containerd.runtimes.kata-runtime]
   runtime_type = "io.containerd.kata.v2"
   privileged_without_host_devices = true

END

rm /usr/bin/docker-runc
ln -s /var/lib/rancher/k3s/data/current/bin/runc /usr/bin/docker-runc

/etc/init.d/k3s start
fi

if [ -e /tmp/smarter.bash ]; then
    cd /tmp
    uudecode /tmp/smarter.bash || true
fi

# Cleanup
rm -f /tmp/smarter.bash
if [ -e /boot/smarter.cfg ]; then
    rm /boot/smarter.cfg
fi
if [ -e $BYLABEL/SMARTER-CFG ]; then
    dd if=/dev/zero of=$BYLABEL/SMARTER-CFG bs=4096 count=1
fi
