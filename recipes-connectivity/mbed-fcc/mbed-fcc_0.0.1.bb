DESCRIPTION = "FCC tool"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/git/LICENSE;md5=4336ad26bb93846e47581adc44c4514d"

DEPENDS = "python-native python python-setuptools-native python-setuptools-scm cmake-native"
RDEPENDS_${PN} += "bash python python-core"

inherit cmake pkgconfig gitpkgv distutils setuptools pythonnative

PV = "1.0+git${SRCPV}"
PKGV = "1.0+git${GITPKGV}"
PR = "r0"
SRCREV = "4.4.0"

SRC_URI = "git://git@github.com/ARMmbed/factory-configurator-client-example.git;protocol=https \
file://0001-fix-build-getting-cross-compiler-iface-setting-to-et.patch \
"
S = "${WORKDIR}/git"
FILES_${PN} = "/opt/arm/factory-configurator-client-armcompiled.elf"

do_configure () {
	cd ${S}
	curl -k https://bootstrap.pypa.io/pip/2.7/get-pip.py | python
	export PYTHONPATH=`pwd`/recipe-sysroot-native/user/lib/python2.7
	export PATH=$PYTHONPATH:$PATH
        python -m pip install --force-reinstall pyopenssl==19.1.0
	python -m pip install mbed-cli
	python -m pip install jsonschema \
		jinja2 \ 
		intervaltree \
		mbed_ls \ 
		mbed_host_tests \
		mbed_greentea \
		fuzzywuzzy \  
		pyelftools \ 
		manifest_tool \ 
		icetea \ 
		pycryptodome \ 
		pyusb \ 
		cmsis_pack_manager
}

do_compile() {
	cd ${S}
	BUILD_TYPE=${1:-DEBUG}
	mbedpath=$(which mbed);
	python "$mbedpath" deploy
	python pal-platform/pal-platform.py -v deploy --target=Yocto_Generic_YoctoLinux_mbedtls generate
	cd __Yocto_Generic_YoctoLinux_mbedtls/
	export ARMGCC_DIR=$(realpath $(pwd)/../../recipe-sysroot-native/usr/)
	cmake -G 'Unix Makefiles' -DCMAKE_BUILD_TYPE="$BUILD_TYPE" -DCMAKE_TOOLCHAIN_FILE=../pal-platform/Toolchain/POKY-GLIBC/POKY-GLIBC.cmake -DEXTARNAL_DEFINE_FILE=../linux-config.cmake
	make factory-configurator-client-example.elf
}

do_install() {
	install -d "${D}/opt/arm"
	install -m 755 ${S}/__Yocto_Generic_YoctoLinux_mbedtls/factory-configurator-client-example.elf ${D}/opt/arm/factory-configurator-client-armcompiled.elf
}
