# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# Unable to find any files that looked like license statements. Check the accompanying
# documentation and source headers and set LICENSE and LIC_FILES_CHKSUM accordingly.
#
# NOTE: LICENSE is being set to "CLOSED" to allow you to at least start building - if
# this is not accurate with respect to the licensing of the software being built (it
# will not be in most cases) you must specify the correct value before using this
# recipe for anything other than initial testing/development!
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=090d381b4b3eb93194e8cbff4aaae2de"

VERSION = "3.4.0"
BINARY = "yq_linux_amd64"

LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SRC_URI = "https://github.com/mikefarah/yq/releases/download/3.4.0/yq_linux_amd64"
SRC_URI[md5sum] = "84aa6f2c3b40d9233532bb59b1ceef7d"
SRC_URI[sha1sum] = "47c9e142b5eabf36b7d9daf987aa59ccc1e2b566"
SRC_URI[sha256sum] = "f6bd1536a743ab170b35c94ed4c7c4479763356bd543af5d391122f4af852460"
SRC_URI[sha384sum] = "b0e9051d5244a9408deafdbe4d3f0d39d223bbc40d145fd61513a46c45342f2bed15705512295f4903b2f4ba20f205f6"
SRC_URI[sha512sum] = "6e3fc1c56779186386103a4ea326e0cadef27fdf22a2f98f6040643c4587287115d6b449d27ec15ec0b69affdefbf9c053a3b8ed751202ae5eb216732eeff9ec"


inherit native

#S = "${WORKDIR}"

# NOTE: no Makefile found, unable to determine what needs to be done

do_configure () {
	# Specify any needed configure commands here
	:
}

do_compile () {
	# Specify compilation commands here
	:
}

do_install () {
  # Specify install commands here
  install -d ${D}${bindir}
  install ${WORKDIR}/yq_linux_amd64 ${D}${bindir}/yq
}

