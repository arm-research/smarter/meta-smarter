DESCRIPTION = "Image with SMARTER, a cloud native edge foundation.  \
The image includes k3s and necessary support to run in edge environments."

IMAGE_FEATURES += " ssh-server-openssh package-management post-install-logging"

DISTRO_FEATURES += " virtualization kvm"

IMAGE_INSTALL_append = "\
    kernel-modules \
    rng-tools \
    ntp \
    k3s k3s-init ca-certificates jq \
    cni \
    bash \
    usbutils \
    mbed-fcc \
    daemontools \
    cryptsetup \
    btrfs-tools \
    e2fsprogs \
    e2fsprogs-resize2fs \
    dpkg \
    apt \
    sharutils \
    systemd-conf \
    kata-image kata-runtime kata-agent kata-proxy kata-shim kata-kernel \    
    smarter \
    "

LICENSE = "MIT"

PV="v0.6.3"
PR="rc1"

inherit core-image


