DESCRIPTION = " The Command-Line Interface (CLI) part of the Kata Containers runtime component"
HOMEPAGE = "https://github.com/kata-containers/runtime"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://src/github.com/kata-containers/runtime/LICENSE;md5=86d3f3a95c324c9479bd8986968f4327"


FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

GO_IMPORT = "github.com/kata-containers/runtime"
SRCREV = "71ba9f898968a1ff2e727794a4865ccad1cc7b5a"
#SRCREV = "31fcfa6ce289ec1ed23d44318cbb5a5f633d09b0"
SRC_URI = "git://${GO_IMPORT}.git;branch=stable-1.11 \
           file://makefile.patch \
          "
SRC_URI[sha256sum] = "f6bd1536a743ab170b35c94ed4c7c4479763356bd543af5d391122f4af852460"

RDEPENDS_${PN}-dev = ""

DEPENDS = "yq-native \
           qemu \
                 "

RDEPENDS_${PN} = " \
                 qemu \
                 "

# grpc

S = "${WORKDIR}/git"

inherit goarch
inherit go



EXTRA_OEMAKE = "GO='${GO}'"

do_compile() {
	export GOARCH="${TARGET_GOARCH}"
#        export GOROOT="${STAGING_LIBDIR_NATIVE}/${TARGET_SYS}/go"
	# Setup vendor directory so that it can be used in GOPATH.
	#
	# Go looks in a src directory under any directory in GOPATH but netns
	# uses 'vendor' instead of 'vendor/src'. We can fix this with a symlink.
	ln -sfn "${S}/src/${GO_IMPORT}/vendor/" "${S}/src/${GO_IMPORT}/vendor/src"


	export GOPATH="${S}"

	# Pass the needed cflags/ldflags so that cgo
	# can find the needed headers files and libraries
	export CGO_ENABLED="1"
	export CFLAGS=""
	export LDFLAGS=""
        export CGO_CFLAGS="${BUILDSDK_CFLAGS} --sysroot=${STAGING_DIR_TARGET}"
	export CGO_LDFLAGS="${BUILDSDK_LDFLAGS} --sysroot=${STAGING_DIR_TARGET}"

        # Try and put yq into the right place
        mkdir -p ${S}/bin
        cp ${WORKDIR}/recipe-sysroot-native/usr/bin/yq ${S}/bin/yq

	cd ${S}/src/${GO_IMPORT}

	# Static builds work but are not recommended. See Makefile*cgo patch.
	#oe_runmake static
	oe_runmake 
}


do_install() {
	mkdir -p ${D}/${bindir}
	mkdir -p ${D}/${libexecdir}/kata-containers
        
	cp ${WORKDIR}/git/src/${GO_IMPORT}/kata-runtime ${D}/${bindir}
	cp ${WORKDIR}/git/src/${GO_IMPORT}/containerd-shim-kata-v2 ${D}/${bindir}
	cp ${WORKDIR}/git/src/${GO_IMPORT}/kata-netmon ${D}/${libexecdir}/kata-containers




	mkdir -p ${D}/${datadir}/defaults/kata-containers/

	cp ${WORKDIR}/git/src/${GO_IMPORT}/cli/config/configuration-qemu.toml ${D}/${datadir}/defaults/kata-containers/configuration.toml

	sed -e 's| path = .*| path = \"/usr/bin/qemu-system-${TARGET_ARCH}\"|' -i ${D}/${datadir}/defaults/kata-containers/configuration.toml
	sed -e 's/^\(image =.*\)/# \1/g' -i ${D}/${datadir}/defaults/kata-containers/configuration.toml
	sed -e 's/^\(internetworking_model *=.*\)/internetworking_model= \"macvtap\"/g' -i ${D}/${datadir}/defaults/kata-containers/configuration.toml
#        sed -e 's/^# CPU Features.*/cpu_features=\"pmu=off\"/' -i ${D}/${datadir}/defaults/kata-containers/configuration.toml
	sed -e 's/^# *enable_debug *= *true/enable_debug = true/g' -i ${D}/${datadir}/defaults/kata-containers/configuration.toml
	sed -e 's/^# *disable_image_nvdimm .*/disable_image_nvdimm = true/' -i ${D}/${datadir}/defaults/kata-containers/configuration.toml

	# TODO: modify the config file for the configured kernel and fix the location of the qemu-system-binary

}

FILES_${PN} += "${datadir}/defaults/kata-containers/*"

deltask compile_ptest_base
