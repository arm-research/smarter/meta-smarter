DESCRIPTION = " The kata-containers VM kernel"
HOMEPAGE = "https://github.com/kata-containers/packaging/tree/master/kernel#build-kata-containers-kernel"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://src/github.com/kata-containers/packaging/LICENSE;md5=86d3f3a95c324c9479bd8986968f4327"

GO_IMPORT = "github.com/kata-containers/packaging"
SRCREV = "72141e446eb8d5dba041bce2260724e27d60d7a7"
SRC_URI = "git://${GO_IMPORT}.git \
          "

DEPENDS = "coreutils-native bc-native yq-native curl-native bison-native flex-native"

S = "${WORKDIR}/git"

inherit goarch
inherit go

do_configure() {
	export GOARCH="${TARGET_GOARCH}"
	export GOROOT="${STAGING_LIBDIR_NATIVE}/${TARGET_SYS}/go"

	export GOPATH="${S}"
        cd ${S}/src/${GO_IMPORT}/kernel
        CURL_CA_BUNDLE=${STAGING_DIR_NATIVE}/etc/ssl/certs/ca-certificates.crt
        echo $PATH
        export ARCH=arm64
        export CROSS_COMPILE=${WORKDIR}/recipe-sysroot-native/usr/bin/aarch64-poky-linux/aarch64-poky-linux-
	./build-kernel.sh -v 5.4.32 -a ${TARGET_ARCH} -f setup
}

do_compile() {
	export GOARCH="${TARGET_GOARCH}"
	export GOROOT="${STAGING_LIBDIR_NATIVE}/${TARGET_SYS}/go"
	export GOPATH="${S}"
	cd ${S}/src/${GO_IMPORT}/kernel
        export ARCH=arm64
        export CROSS_COMPILE=${WORKDIR}/recipe-sysroot-native/usr/bin/aarch64-poky-linux/aarch64-poky-linux-        
	./build-kernel.sh -v 5.4.32 -a ${TARGET_ARCH} build
}

do_install() {
	export GOARCH="${TARGET_GOARCH}"
	export GOROOT="${STAGING_LIBDIR_NATIVE}/${TARGET_SYS}/go"

	export GOPATH="${S}"
	cd ${S}/src/${GO_IMPORT}/kernel
        export ARCH=arm64
        export CROSS_COMPILE=${WORKDIR}/recipe-sysroot-native/usr/bin/aarch64-poky-linux/aarch64-poky-linux-        
        export DESTDIR=${D}
        ./build-kernel.sh -v 5.4.32 -a ${TARGET_ARCH} install
        find ${D} -type l -exec chown -h root:root {} \;
}


FILES_${PN} += "${datadir}/kata-containers/*"

#deltask compile_ptest_base
