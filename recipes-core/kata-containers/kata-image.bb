DESCRIPTION = " The kata-containers VM RootFS Image"
HOMEPAGE = "https://github.com/kata-containers/packaging/tree/master/kernel#build-kata-containers-kernel"


PV = "v1.1"

# Select pre-compiled package based on target architecture
def kata_pkg_arch_map(d):
    arch = d.getVar('TARGET_ARCH', True)
    tune = d.getVar('TUNE_FEATURES', True)
    tune_features = tune.split()
    if arch == "x86_64":
        if not "mx32" in tune_features:
            pkg = "amd64"
    elif arch == "aarch64" or arch == "arm64":
        pkg = "arm64"
    elif arch == "arm":
        if "callconvention-hard" in tune_features:
            pkg = "armhf"
    
    if not pkg:
        raise bb.parse.SkipPackage("Target architecture '%s' is not supported by the meta-k3s layer" %arch)

    return pkg

KATA_IMAGE_PKG_ARCH = "${@kata_pkg_arch_map(d)}"

SRC_URI="https://gitlab.com/arm-research/smarter/kata-rootfs/-/jobs/1040552616/artifacts/raw/${PV}/${KATA_IMAGE_PKG_ARCH}/kata-containers-initrd.img;unpack=0;name=kata-initrd;downloadfilename=kata-containers-initrd-${KATA_IMAGE_PKG_ARCH}.img \
        https://gitlab.com/arm-research/smarter/kata-rootfs/-/raw/master/LICENSE.md;name=LICENSE"

SRC_URI[kata-initrd.sha256sum] = "703ce61cdf773dba8649cf0276b5565542ebcc6f3fd8ad8eaedacf09581f1ee5"
SRC_URI[LICENSE.sha256sum] = "89c03adedb6c4146cfa58d43a46b1ec058aefb1e909da89cf781d7246567ece3"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENSE.md;md5=f794134c8808282f1a6a43af1c03512d"


do_install() {
      DESTDIR=${D}
      install -o root -g root -m 0640 -D ${WORKDIR}/kata-containers-initrd-${KATA_IMAGE_PKG_ARCH}.img "${D}/usr/share/kata-containers/kata-containers-initrd.img"
}


FILES_${PN} += "${datadir}/kata-containers/*"

#deltask compile_ptest_base
