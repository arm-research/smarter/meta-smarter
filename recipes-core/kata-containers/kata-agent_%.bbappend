FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRCREV = "801e69daf7e9f21fdcef76fcfcf69239c9881381"
SRC_URI = "git://${GO_IMPORT}.git;branch=stable-1.11 \
          "

RDEPENDS_${PN}-dev = ""

S = "${WORKDIR}/git"

inherit go
inherit goarch


do_compile() {
	# Pass the needed cflags/ldflags so that cgo
	# can find the needed headers files and libraries
	export GOARCH=${TARGET_GOARCH}
	export CGO_ENABLED="1"
	export CGO_CFLAGS="${CFLAGS} --sysroot=${STAGING_DIR_TARGET}"
	export LDFLAGS=""


	cd ${S}/src/${GO_IMPORT}
	oe_runmake kata-agent
}

do_install() {
        install -d ${D}${bindir}
        install -m 0755 ${WORKDIR}/git/src/${GO_IMPORT}/kata-agent ${D}${bindir}

        mkdir -p ${D}/${systemd_unitdir}/system
	cp ${WORKDIR}/git/src/${GO_IMPORT}/kata-agent.service ${D}/${systemd_unitdir}/system
}

deltask compile_ptest_base

FILES_${PN} = "${bindir}/kata-agent ${systemd_unitdir}/system/*"




