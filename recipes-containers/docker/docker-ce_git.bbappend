# Remove the exclude_graphdriver_btrfs
BUILD_TAGS = "exclude_graphdriver_devicemapper"
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

do_install_append() {
     if ${@bb.utils.contains('DISTRO_FEATURES', 'sysvinit', 'true', 'false', d)};    then
 # comment out entire init file so docker is not started by default    
 	sed -e 's/^/#/g' -i         ${D}${sysconfdir}/init.d/docker.init
     fi
}
