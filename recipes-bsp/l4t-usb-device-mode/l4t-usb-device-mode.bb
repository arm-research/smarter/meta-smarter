SUMMARY = "Installs tools to control tegra usb OTG device"
LIC_FILES_CHKSUM = "file://LICENSE.filesystem.img;md5=ca1e4f3f7a08a162b02161606d2fb3ee"
LICENSE = "Proprietary"

RDEPENDS_${PN} = "bash \
                 dhcp-server \
                 ${PN}-runtime \
"


SRC_URI = "file://LICENSE.filesystem.img \
           file://mac-addresses \
           file://nv-l4t-usb-device-mode-runtime-start.sh \
           file://nv-l4t-usb-device-mode-runtime.service \
           file://nv-l4t-usb-device-mode-state-change.sh \
           file://nv-l4t-usb-device-mode.service \
           file://nv-l4t-usb-device-mode-config.sh \
           file://nv-l4t-usb-device-mode-runtime-stop.sh \
           file://nv-l4t-usb-device-mode-start.sh \
           file://nv-l4t-usb-device-mode-stop.sh \
           file://99-nv-l4t-usb-device-mode.rules \
"

S = "${WORKDIR}"

inherit systemd

do_install() {
  install -d ${D}/${systemd_system_unitdir}
  install -m 0644 ${S}/nv-l4t-usb-device-mode.service ${D}${systemd_system_unitdir}/
  install -m 0644 ${S}/nv-l4t-usb-device-mode-runtime.service ${D}${systemd_system_unitdir}/

  install -d ${D}/opt/nvidia/l4t-usb-device-mode
  install -m 0644 ${WORKDIR}/LICENSE.filesystem.img ${D}/opt/nvidia/l4t-usb-device-mode
  install -m 0644 ${WORKDIR}/mac-addresses ${D}/opt/nvidia/l4t-usb-device-mode
  install -m 0750 ${WORKDIR}/nv-l4t-usb-device-mode-runtime-start.sh ${D}/opt/nvidia/l4t-usb-device-mode
  install -m 0750 ${WORKDIR}/nv-l4t-usb-device-mode-state-change.sh ${D}/opt/nvidia/l4t-usb-device-mode
  install -m 0750 ${WORKDIR}/nv-l4t-usb-device-mode-config.sh ${D}/opt/nvidia/l4t-usb-device-mode
  install -m 0750 ${WORKDIR}/nv-l4t-usb-device-mode-runtime-stop.sh ${D}/opt/nvidia/l4t-usb-device-mode
  install -m 0750 ${WORKDIR}/nv-l4t-usb-device-mode-start.sh ${D}/opt/nvidia/l4t-usb-device-mode
  install -m 0750 ${WORKDIR}/nv-l4t-usb-device-mode-stop.sh ${D}/opt/nvidia/l4t-usb-device-mode

  install -d ${D}/${sysconfdir}/udev/rules.d
  install -m 0644 99-nv-l4t-usb-device-mode.rules ${D}${sysconfdir}/udev/rules.d/99-nv-l4t-usb-device-mode.rules


}
SYSTEMD_PACKAGES += "${PN} ${PN}-runtime"
PACKAGES = "${PN} ${PN}-runtime"

SYSTEMD_SERVICE_${PN} = "nv-l4t-usb-device-mode.service"
SYSTEMD_SERVICE_${PN}-runtime = "nv-l4t-usb-device-mode-runtime.service"
SYSTEMD_AUTO_ENABLE_${PN}-runtime = "disable"

FILES_${PN} += " /opt"
FILES_${PN} += " ${sysconfdir}/udev"

