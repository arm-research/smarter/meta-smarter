SMD_CFG = "${S}/smd_info_AB.cfg"

do_install_append_tegra194() {
    install -m 0644 ${S}/flash_ota_encrypted_t194.xml ${D}${datadir}/tegraflash/flash_ota_encrypted_${MACHINE}.xml
    install -m 0644 ${S}/doflash_ota.sh ${D}${datadir}/tegraflash/doflash_ota.sh
}

