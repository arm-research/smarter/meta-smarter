#!/usr/bin/env python

import os, ctypes, sys, time

libc = ctypes.CDLL(None)
_syscall_reboot = libc.syscall
_syscall_reboot.argtypes = [ctypes.c_long, ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_char_p]

def reboot(arg):
    buf = ctypes.c_char_p(arg)

    result = _syscall_reboot(142,0xfee1dead,672274793,0xa1b2c3d4,buf) # LINUX_REBOOT_CMD_RESTART2 and LINUX_REBOOT_MAGIC2 (reboots but does not enter recovery)

    if result < 0:
        raise OSError(ctypes.get_errno(), 'reboot() failed')
    return result

if len(sys.argv) < 2:
    print("Timeout should be provided")
    sys.exit(1)

timeout=int(sys.argv[1])

print("Timeout of "+str(timeout)+"s")
time.sleep(timeout)
print("Rebooting now")

print(reboot("forced-recovery"))
