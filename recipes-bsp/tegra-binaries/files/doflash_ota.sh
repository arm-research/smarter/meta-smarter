#!/bin/sh
PATH=$PATH:tegra186-flash
#########################
# The command line help #
#########################
display_help() {
    echo "Usage: $0 " >&2
    echo
    echo "   -u, --url     sets the vault url"
    echo "                 e.g.https://sharedservices.azure-api.net/v1/insight/data/disk"
    echo "   -t, --token   Set authentication token to get access to the key "
    exit 1
}

umount_safe() {
    set +e
    while true; do
        if sudo umount mount/ ; then
          echo Mount point succesfully umounted
          break
        else
          sleep 1
        fi
    done
    set -e
}

# argument $1 as $TOKEN
# argument $2 as URL
write_token_to_image() {
    mkdir -p mount
    echo Running sudo mount for the ext4 partition
    sudo mount persistent_part.img mount/
    echo Adding vault key to the mounted image
    echo TOKEN=\"$1\" | sudo tee mount/vault_env >/dev/null
    echo URL=\"$2\" | sudo tee -a mount/vault_env >/dev/null
    sudo chmod 600 mount/vault_env
    echo Running sudo unmount
    umount_safe
    echo Clean up
    rm -rf mount
}

if ! which jq >/dev/null; then
    echo "'jq' was not found, please install jq first"
    exit 1
fi

for arg in "$@"
do
    case $arg in
        -t|--token)
        TOKEN="$2"
        shift
        shift
        ;;
        -u|--url)
        URL="$2"
        shift
        shift
        ;;
        -h | --help)
        display_help
        exit 0
        ;;
    esac
done

if [ -z "$TOKEN" ] || [ -z "$URL" ]
then
  display_help
else
    echo "Adding insight partition to the setup encrypted with the password you specified"
    fallocate -l REMAINING_SPACE insight_part.img
    echo "Getting the encryption password from the vault"
    PASSWORD=$(curl -s --header "X-Vault-Token: $TOKEN" $URL | jq -r '.data.data.disk')
    echo -n $PASSWORD | cryptsetup --batch-mode luksFormat insight_part.img -d -
    img2simg insight_part.img insight_part.simg
    mv insight_part.simg insight_part.img

    echo "Creating persistent space"
    rm -f persistent_part.img
    fallocate -l PERSISTENT_SIZE persistent_part.img
    mkfs.ext4 persistent_part.img

    echo "Writing token to image"
    write_token_to_image $TOKEN $URL
    ./tegra186-flash/tegra194-flash-helper.sh flash.xml.in DTBFILE MACHINE.cfg,MACHINE-override.cfg ODMDATA
fi

