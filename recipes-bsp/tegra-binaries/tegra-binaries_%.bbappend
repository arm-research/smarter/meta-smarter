FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += "\
  file://nvpmodel_gateway_t194.conf \
  file://jetson_clocks.service \
  file://flash_ota_encrypted_t194.xml \
  file://doflash_ota.sh \
  file://smd_info_AB.cfg \
  file://nvidia_recovery.py \
"

do_preconfigure_append() {
  cp ${WORKDIR}/nvpmodel_gateway_t194.conf ${S}/
  cp ${WORKDIR}/jetson_clocks.service ${S}/
  cp ${WORKDIR}/flash_ota_encrypted_t194.xml ${S}/
  cp ${WORKDIR}/doflash_ota.sh ${S}/
  cp ${WORKDIR}/smd_info_AB.cfg ${S}/
  cp ${WORKDIR}/nvidia_recovery.py ${S}/
}
