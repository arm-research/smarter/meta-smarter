do_install_append() {
    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${S}/jetson_clocks.service ${D}${systemd_system_unitdir}/
}

PACKAGES += "${PN}-jetson-clocks"
FILES_${PN}-jetson-clocks = "${systemd_system_unitdir}"
SYSTEMD_PACKAGES += " ${PN}-jetson-clocks"
SYSTEMD_SERVICE_${PN}-jetson-clocks = "jetson_clocks.service"
