do_install_append() {
    install -m 0755 ${S}/nvidia_recovery.py ${D}${bindir}/
}

PACKAGES += "${PN}-nvidia-recovery"
FILES_${PN}-nvidia-recovery = "${bindir}/nvidia_recovery.py"
RDEPENDS_${PN}-nvidia-recovery = "python python-core"
