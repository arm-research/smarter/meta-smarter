SRCREV_meta = "aafb8f095e97013d6e55b09ed150369cbe0c6476"
FILESEXTRAPATHS_prepend:="${THISDIR}/files:"

SRC_URI_append += " \
    git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;name=meta;branch=yocto-5.4;destsuffix=kernel-meta \
    file://vault.cfg \
    file://additional_docker.cfg \
    "

do_configure_append() {
    ${S}/scripts/kconfig/merge_config.sh -m -O ${B} ${B}/.config ${WORKDIR}/*.cfg
}

CMDLINE_append += " fixrtc cgroup_enable=cpuset cgroup_enable=memory cgroup_memory=1"